源码地址：https://github.com/JetBrains/compose-jb

原仓库地址：https://maven.pkg.jetbrains.space/public/p/compose/dev

阿里云托管地址：https://code.aliyun.com/maven-mirror/compose-jb-dev-repo/tree/master/repo

gitlab托管地址：https://gitlab.com/maven-mirror/compose-jb-dev-repo

### 使用方式

##### `gradle`
```gradle
repositories {
    maven {
        // 阿里云
        url "https://code.aliyun.com/maven-mirror/compose-jb-dev-repo/raw/master/repo"
        
        // gitlab
        url "https://gitlab.com/maven-mirror/compose-jb-dev-repo/-/raw/master/repo"
    }
}
```

#### `maven`
```xml
    略...
```
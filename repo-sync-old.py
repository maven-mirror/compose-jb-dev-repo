#!/usr/bin/env python3
#
# pip3 install download
# pip3 install bs4
# pip3 install best-download
#

import http
import http.client
import requests
from html.parser import HTMLParser
from bs4 import BeautifulSoup
import os
import re
import time
from download import download
import sys
import getopt
from concurrent.futures import ThreadPoolExecutor
import threading
import ssl
from best_download import download_file

homePageUrl = "https://maven.pkg.jetbrains.space/public/p/compose/dev/"
parentPagePath = "../"
localRepoDirPath = os.getcwd() + "/repo"
localRepoSyncRecordFilePath = localRepoDirPath+"/.repo"
repoFileItemList = []
isSyncForce = False
targetLib = None

# 重写Https默认的校验方式,避免ssl验证失败
ssl._create_default_https_context = ssl._create_unverified_context


class RepoManager():
    def __init__(self):
        print("初始化配置...")
        # repo日志写入器
        self._localRepoLogWriter = open(localRepoSyncRecordFilePath, 'a+')
        # 追加下载记录的锁
        self._appendDownloadHistoryLock = threading.Lock()
        # 下载线程的线程池
        self._downloadThreadPool = ThreadPoolExecutor(max_workers=16)
        # 寻找线程的线程池
        self._findThreadPool = ThreadPoolExecutor(max_workers=8)
        # 线程池中所有的元素
        self._allThreadPoolItems = []

    def sync(self):
        # 提交查找任务到队列中
        self.submitFindThreadPool(homePageUrl, localRepoDirPath)
        print("正在准备执行...")
        # time.sleep(1000)
        while len(self._allThreadPoolItems) > 0:
            poolItem = self._allThreadPoolItems[0]
            if poolItem.done():
                self._allThreadPoolItems.remove(poolItem)
        # 关闭线程池
        self._findThreadPool.shutdown()
        self._downloadThreadPool.shutdown()

    # 提交查找任务到队列中
    def submitFindThreadPool(self, pageUrl, basePath):
        # 提交查找任务到队列中
        # future = self._findThreadPool.submit(
        #     self.findRepoFiles, pageUrl, basePath)
        # self._allThreadPoolItems.append(future)
        self.findRepoFiles(pageUrl, basePath)

    # 提交下载任务到队列中
    def submitDownloadThreadPool(self, repoFileItem):
        # 提交下载任务到队列中
        # future = self._downloadThreadPool.submit(
        #     saveRepoFileItem, repoFileItem)
        # self._allThreadPoolItems.append(future)
        saveRepoFileItem(repoFileItem)

    def findRepoFiles(self, pageUrl, basePath):
        response = requests.get(pageUrl)
        if response.status_code == 200:
            # soup = BeautifulSoup(response.text, "html.parser", from_encoding=response.encoding.lower())
            soup = BeautifulSoup(response.text, "html.parser",
                                 exclude_encodings=response.encoding.lower())
            tags = soup.select("main pre[id=contents] a")
            for tag in tags:
                if parentPagePath.__eq__(tag.string):
                    continue
                href = tag['href']
                if href.endswith("/"):  # 目录
                    # 提交查找任务到队列中
                    self.submitFindThreadPool(
                        href, autoCreateDir(basePath, tag.string))
                else:  # 文件，需要下载
                    # 开始匹配下载对应的文件
                    fileInfoText = tag.find_next_siblings(text=True)[0].strip()
                    repoFileItem = RepoFileItem(href, tag['title'],
                                                os.path.join(
                                                    basePath, tag.string),
                                                int(time.mktime(time.strptime(parserDateFromText(fileInfoText),
                                                                              "%Y-%m-%d %H:%M")) * 1000),
                                                int(parserFileSizeFromText(fileInfoText), base=10))
                    # 提交下载任务到队列中
                    self.submitDownloadThreadPool(repoFileItem)
                    # print("" + tag.string)
                    # repoFileItemList.append()


class RepoSyncResult:
    def __init__(self, url, name, time, size):
        self.url = url
        self.time = time
        self.size = size


class RepoFileItem:
    def __init__(self, url, name, filePath, time, size):
        self.url = url
        self.name = name
        self.filePath = filePath
        self.time = time
        self.size = size


# 从字符串读取日期信息
def parserDateFromText(text):
    pattern = r'(\d{4}-\d{1,2}-\d{1,2} \d{1,2}:\d{1,2})'
    pattern = re.compile(pattern)
    result = pattern.findall(text)
    if len(result) > 0:
        return result[0]
    return text


# 从字符串读取文件大小信息
def parserFileSizeFromText(text):
    pattern = r'(\d+\d*)'
    pattern = re.compile(pattern)
    result = pattern.findall(text)
    if len(result) > 0:
        return result[len(result) - 1]
    return text


# 自动创建目录
def autoCreateDir(basePath, dirPath):
    realPath = os.path.join(basePath, dirPath)
    if not os.path.exists(realPath):
        os.mkdir(realPath)
    return realPath


# 保存maven文件（内容，时间等信息）
def saveRepoFileItem(repoFileItem):
    # print("\n")
    # print("url=", repoFileItem.url)
    # print("name=", repoFileItem.name)
    # print("filePath=", repoFileItem.filePath)
    # print("time=", repoFileItem.time)
    # print("size=", repoFileItem.size)
    # fileTime = os.path.getmtime(repoFileItem.path)
    # print(fileTime)
    modifyTime = repoFileItem.time / 1000
    try:
        if isSyncForce:
            # print("----------- 开始下载 -----------")
            print("开始下载：", repoFileItem.url)
            if os.path.exists(repoFileItem.filePath):
                os.remove(repoFileItem.filePath)
            # download_file(repoFileItem.url, repoFileItem.filePath)
            download(repoFileItem.url, repoFileItem.filePath,
                     progressbar=True, replace=True)
        elif not os.path.exists(repoFileItem.filePath):
            # print("----------- 开始下载 -----------")
            print("开始下载：", repoFileItem.url)
            # download_file(repoFileItem.url, repoFileItem.filePath)
            download(repoFileItem.url, repoFileItem.filePath,
                     progressbar=True, replace=False)
        else:
            print("已下载：", repoFileItem.url)
        os.utime(repoFileItem.filePath, (modifyTime, modifyTime))
        # print("----------- 结束下载 -----------")
    except BaseException as baseException:
        print(baseException)
        print("文件下载中断！", repoFileItem.url)
        if os.path.exists(repoFileItem.filePath):
            os.remove(repoFileItem.filePath)


def recordSyncResult(repoFileItem):
    with open(file, 'a+') as f:
        f.write(j+'\n')


def printHelpInfo():
    print('mirrord.py [-f|--force] [-t|--target <lib>]')


# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    try:
        opts, args = getopt.getopt(sys.argv, "hi:o:", ["ifile=", "ofile="])
    except getopt.GetoptError:
        printHelpInfo()
        sys.exit(2)
    for opt, arg in opts:
        if opt in ("-h", "--help"):
            printHelpInfo()
            sys.exit()
        elif opt in ("-f", "--force"):
            isSyncForce = True
        elif opt in ("-t", "--target"):
            targetLib = arg
        else:
            printHelpInfo()
    if not os.path.exists(localRepoDirPath):
        os.mkdir(localRepoDirPath)
    RepoManager().sync()
    print("操作完成！")

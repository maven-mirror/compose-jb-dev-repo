#!/usr/bin/env python3
#
# pip3 install download
# pip3 install bs4
# pip3 install best-download
#

import http
import http.client
import requests
from html.parser import HTMLParser
from bs4 import BeautifulSoup
import os
import re
import time
from download import download
import sys
from concurrent.futures import ThreadPoolExecutor
import threading
import ssl
from best_download import download_file
import signal
import json

homePageUrl = "https://maven.pkg.jetbrains.space/public/p/compose/dev"
parentPagePath = "../"
localRepoDirPath = os.getcwd() + "/repo"
localRepoSyncRecordFilePath = localRepoDirPath+"/.repo"
repoFileItemList = []
isSyncForce = False
targetLib = None
isUseMultiThreadDownload = True  # 是否开启多线程下载,可用于下载大文件
MAX_DOWNLOAD_THREADS = 16  # 最大下载线程数
MAX_SEARCH_THREADS = 8  # 最大搜索线程数
MINI_TRUCK_FILE_SIZE = 1024*500  # 500Kb是需要分包下载的文件的最小值

# 重写Https默认的校验方式,避免ssl验证失败
ssl._create_default_https_context = ssl._create_unverified_context


class RepoManager():
    def __init__(self):
        print("初始化配置...")
        if os.path.exists(localRepoSyncRecordFilePath):
            os.remove(localRepoSyncRecordFilePath)
        # repo日志写入器
        self._localRepoLogWriter = open(localRepoSyncRecordFilePath, 'a+')
        # repo日志写入器
        self._localRepoLogWriterLock = threading.Lock()
        # 追加下载记录的锁
        self._appendDownloadTaskLock = threading.Lock()
        # 下载线程的线程池
        self._downloadThreadPool = ThreadPoolExecutor(
            max_workers=MAX_DOWNLOAD_THREADS)
        # 寻找线程的线程池
        self._findThreadPool = ThreadPoolExecutor(
            max_workers=MAX_SEARCH_THREADS)
        # 存放所有线程的句柄
        self._allThreadPoolItems = []
        # 等待下载的队列
        self._downloadTasks = []
        self.isCancel = False
        # 下载出错的项
        self._downloadErrorItems = []

    def _cancel(self):
        self.isCancel = True
        print("正在退出...")
        # 关闭查询线程池
        self._findThreadPool.shutdown()
        # 关闭下载线程池
        self._downloadThreadPool.shutdown()
        self._appendDownloadTaskLock.acquire()
        self._downloadTasks.clear()
        self._appendDownloadTaskLock.release()
        self._allThreadPoolItems.clear()
        os._exit(0)

    def sync(self):
        signal.signal(signal.SIGINT, self._cancel)
        signal.signal(signal.SIGTERM, self._cancel)
        # 提交查找任务到队列中
        self.submitFindTask(homePageUrl, localRepoDirPath)
        print("正在准备执行...")
        # 等待所有下载完毕
        while len(self._downloadTasks) > 0 or len(self._allThreadPoolItems) > 0:
            if len(self._allThreadPoolItems) > 0:
                poolItem = self._allThreadPoolItems[0]
                if poolItem.done():
                    self._allThreadPoolItems.remove(poolItem)
            if len(self._downloadTasks) > 0:
                repoFileItem = self._downloadTasks[0]
                print("\n开始下载：", repoFileItem.url)
                # repo是否大于最小值
                isRepoFileSizeMoreThanMiniSize = repoFileItem.size > MINI_TRUCK_FILE_SIZE
                if isSyncForce:
                    if isUseMultiThreadDownload or isRepoFileSizeMoreThanMiniSize:
                        self.downloadFileCompat(repoFileItem, True)
                    else:
                        self._allThreadPoolItems.append(self._downloadThreadPool.submit(
                            self.downloadFileCompat, repoFileItem, True))
                elif not os.path.exists(repoFileItem.filePath):
                    if isUseMultiThreadDownload or isRepoFileSizeMoreThanMiniSize:
                        self.downloadFileCompat(repoFileItem, False)
                    else:
                        self._allThreadPoolItems.append(self._downloadThreadPool.submit(
                            self.downloadFileCompat, repoFileItem, False))
                else:
                    print("\n已下载：", repoFileItem.url)
                # 从下载任务中删除
                self._appendDownloadTaskLock.acquire()
                self._downloadTasks.remove(repoFileItem)
                self._appendDownloadTaskLock.release()
        # 关闭查询线程池
        self._findThreadPool.shutdown()
        # 关闭下载线程池
        self._downloadThreadPool.shutdown()
        print('\n')
        if len(self._downloadErrorItems) > 0:
            for item in self._downloadErrorItems:
                print(f"下载失败：{item.url}")
            print(f"共发现{len(self._downloadErrorItems)}个错误项目")
        else:
            print("全部下载完成！")

    # 提交查找任务到队列中

    def submitFindTask(self, pageUrl, basePath):
        # 提交查找任务到队列中
        future = self._findThreadPool.submit(
            self.findRepoFiles, pageUrl, basePath)
        self._allThreadPoolItems.append(future)
        # self.findRepoFiles(pageUrl, basePath)

    # 直接下载文件
    def downloadFileCompat(self, repoFileItem, isReplace):
        if self.isCancel:
            return
        try:
            if os.path.exists(repoFileItem.filePath):
                if isReplace:
                    os.remove(repoFileItem.filePath)
                elif os.stat(repoFileItem.filePath).st_size != repoFileItem.size:  # 文件大小不匹配需要重新下载
                    os.remove(repoFileItem.filePath)
            modifyTime = repoFileItem.time / 1000
            if isUseMultiThreadDownload:
                download_file(repoFileItem.url, repoFileItem.filePath)
            else:
                download(repoFileItem.url, repoFileItem.filePath,
                         progressbar=True, replace=isReplace)
            os.utime(repoFileItem.filePath, (modifyTime, modifyTime))
            self._localRepoLogWriterLock.acquire()
            jsonData = {
                'url': repoFileItem.url,
                'time': repoFileItem.time,
                'size': repoFileItem.size,
            }
            self._localRepoLogWriter.write(json.dumps(jsonData)+'\n')
            self._localRepoLogWriter.flush()
            self._localRepoLogWriterLock.release()
            print("下载完成！", repoFileItem.url)
        except BaseException as baseException:
            print(baseException)
            print("文件下载中断！", repoFileItem.url)
            if os.path.exists(repoFileItem.filePath):
                os.remove(repoFileItem.filePath)
            self._downloadErrorItems.append(repoFileItem)

    def findRepoFiles(self, pageUrl, basePath):
        if self.isCancel:
            return
        response = requests.get(pageUrl)
        if response.status_code == 200:
            # soup = BeautifulSoup(response.text, "html.parser", from_encoding=response.encoding.lower())
            soup = BeautifulSoup(response.text, "html.parser",
                                 exclude_encodings=response.encoding.lower())
            tags = soup.select("main pre[id=contents] a")
            for tag in tags:
                if parentPagePath.__eq__(tag.string):
                    continue
                href = tag['href']
                if href.endswith("/"):  # 目录
                    # 提交查找任务到队列中
                    self.submitFindTask(
                        href, autoCreateDir(basePath, tag.string))
                else:  # 文件，需要下载
                    # 开始匹配下载对应的文件
                    fileInfoText = tag.find_next_siblings(text=True)[0].strip()
                    repoFileItem = RepoFileItem(href, tag['title'],
                                                os.path.join(
                                                    basePath, tag.string),
                                                int(time.mktime(time.strptime(parserDateFromText(fileInfoText),
                                                                              "%Y-%m-%d %H:%M")) * 1000),
                                                int(parserFileSizeFromText(fileInfoText), base=10))
                    # 提交下载任务到队列中
                    self._appendDownloadTaskLock.acquire()
                    self._downloadTasks.append(repoFileItem)
                    self._appendDownloadTaskLock.release()


class RepoSyncResult:
    def __init__(self, url, name, time, size):
        self.url = url
        self.time = time
        self.size = size


class RepoFileItem:
    def __init__(self, url, name, filePath, time, size):
        self.url = url
        self.name = name
        self.filePath = filePath
        self.time = time
        self.size = size


# 从字符串读取日期信息
def parserDateFromText(text):
    pattern = r'(\d{4}-\d{1,2}-\d{1,2} \d{1,2}:\d{1,2})'
    pattern = re.compile(pattern)
    result = pattern.findall(text)
    if len(result) > 0:
        return result[0]
    return text


# 从字符串读取文件大小信息
def parserFileSizeFromText(text):
    pattern = r'(\d+\d*)'
    pattern = re.compile(pattern)
    result = pattern.findall(text)
    if len(result) > 0:
        return result[len(result) - 1]
    return text


# 自动创建目录
def autoCreateDir(basePath, dirPath):
    realPath = os.path.join(basePath, dirPath)
    if not os.path.exists(realPath):
        os.mkdir(realPath)
    return realPath


# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    inputResult = input("是否开启分块下载？(y/n,默认n)：").lower()
    if inputResult == '':
        inputResult = 'n'
    isUseMultiThreadDownload = 'y' == inputResult
    inputResult = input(f"下载线程数(默认{MAX_DOWNLOAD_THREADS})：")
    if inputResult != '':
        MAX_DOWNLOAD_THREADS = int(inputResult)
    inputResult = input(f"搜索线程数(默认{MAX_SEARCH_THREADS})：")
    if inputResult != '':
        MAX_SEARCH_THREADS = int(inputResult)
    inputResult = input(f"需要进行分包下载的文件最小长度(默认{MINI_TRUCK_FILE_SIZE}字节)：")
    if inputResult != '':
        MINI_TRUCK_FILE_SIZE = int(inputResult)
    if not os.path.exists(localRepoDirPath):
        os.mkdir(localRepoDirPath)
    RepoManager().sync()
    print("操作完成！")
